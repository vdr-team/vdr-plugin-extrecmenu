vdr-plugin-extrecmenu (1.2.4+git20171227-2) experimental; urgency=medium

  * VCS moved to salsa.debian.org
  * Build-depend on vdr-dev (>= 2.4.0)

 -- Tobias Grimm <etobi@debian.org>  Mon, 16 Apr 2018 19:35:10 +0200

vdr-plugin-extrecmenu (1.2.4+git20171227-1) experimental; urgency=medium

  * New Upstream Snapshot (commit 77d10fa)
  * Standards-Version: 4.1.2
  * Priority: optional

 -- Tobias Grimm <etobi@debian.org>  Wed, 27 Dec 2017 15:52:54 +0100

vdr-plugin-extrecmenu (1.2.4-4) experimental; urgency=medium

  * Now supporting the /etc/vdr/conf.d mechanism

 -- Tobias Grimm <etobi@debian.org>  Mon, 23 Mar 2015 22:44:22 +0100

vdr-plugin-extrecmenu (1.2.4-3) experimental; urgency=medium

  * Build-depend on vdr-dev (>= 2.2.0)

 -- Tobias Grimm <etobi@debian.org>  Thu, 19 Feb 2015 12:39:46 +0100

vdr-plugin-extrecmenu (1.2.4-2) experimental; urgency=medium

  * Build-depend on vdr-dev (>= 2.1.10)
  * Standards-Version: 3.9.6

 -- Tobias Grimm <etobi@debian.org>  Sun, 15 Feb 2015 19:32:45 +0100

vdr-plugin-extrecmenu (1.2.4-1) experimental; urgency=medium

  * New upstream release

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Feb 2015 19:37:14 +0100

vdr-plugin-extrecmenu (1.2.2+git20130323-3) experimental; urgency=low

  * Build-depend on vdr-dev (>= 2.0.0)

 -- Tobias Grimm <etobi@debian.org>  Sun, 31 Mar 2013 13:58:53 +0200

vdr-plugin-extrecmenu (1.2.2+git20130323-2) experimental; urgency=low

  * Build-depend on vdr-dev (>= 1.7.42)

 -- Tobias Grimm <etobi@debian.org>  Sat, 23 Mar 2013 20:51:59 +0100

vdr-plugin-extrecmenu (1.2.2+git20130323-1) experimental; urgency=low

  * New Upstream Snapshot (commit 16da1f9)
  * Standards-Version: 3.9.4
  * Build-depend in vdr-dev (>= 1.7.41)
  * Use debhelper 9

 -- Tobias Grimm <etobi@debian.org>  Sat, 23 Mar 2013 09:47:14 +0100

vdr-plugin-extrecmenu (1.2.2-2) experimental; urgency=low

  * Build-depend on vdr >= 1.7.28

 -- Tobias Grimm <etobi@debian.org>  Fri, 08 Jun 2012 15:24:45 +0200

vdr-plugin-extrecmenu (1.2.2-1) experimental; urgency=low

  * New upstream release

 -- Tobias Grimm <etobi@debian.org>  Sun, 06 May 2012 14:05:30 +0200

vdr-plugin-extrecmenu (1.2.1+git20110921-2) experimental; urgency=low

  * Switched to GIT using pristine tar

 -- Tobias Grimm <etobi@debian.org>  Sat, 29 Oct 2011 19:51:03 +0200

vdr-plugin-extrecmenu (1.2.1+git20110921-1) experimental; urgency=low

  * New Upstream Snapshot (commit 02265ea)
  * New upstream location, updated copyright and debian/watch
  * Standards-Version: 3.9.2
  * Dropped 90_extrecmenu-1.2-test1-am2b.dpatch
  * Dropped 91_vdr-1.7.12-extrec.dpatch
  * Source format 3.0 (quilt)
  * Switched to debhelper 7 and dropped cdbs
  * Build-depend on vdr-dev (>= 1.7.21)

 -- Tobias Grimm <etobi@debian.org>  Wed, 21 Sep 2011 20:13:24 +0200

vdr-plugin-extrecmenu (1.2~test1-3) experimental; urgency=low

  * Added 91_vdr-1.7.12-extrec.dpatch
  * Standards-Version: 3.8.4

 -- Thomas Günther <tom@toms-cafe.de>  Mon, 15 Mar 2010 18:05:54 +0100

vdr-plugin-extrecmenu (1.2~test1-2) experimental; urgency=low

  * Removed non-standard shebang line from debian/rules
  * Added README.source
  * Standards-Version: 3.8.3

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Nov 2009 19:36:23 +0100

vdr-plugin-extrecmenu (1.2~test1-1) experimental; urgency=low

  * New upstream test version downloaded from
    http://martins-kabuff.de/download/vdr-extrecmenu-1.2-test1.tgz
  * Added 90_extrecmenu-1.2-test1-am2b.dpatch
  * Added gettext to Build-Depends
  * Added installation of locale files
  * Added ${misc:Depends}
  * Bumped standards version to 3.8.1
  * Updated debian/copyright
  * Changed section to "video"
  * Removed debian/docs (not necessary with cdbs)
  * Removed DVBDIR from debian/rules

 -- Thomas Günther <tom@toms-cafe.de>  Fri, 31 Jul 2009 00:07:13 +0200

vdr-plugin-extrecmenu (1.1-8) experimental; urgency=low

  * Dropped patchlevel control field
  * Build-Depend on vdr-dev (>=1.6.0-5)
  * Bumped Standards-Version to 3.8.0

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 25 Jul 2008 19:24:40 +0200

vdr-plugin-extrecmenu (1.1-7) experimental; urgency=low

  * Increased package version to force rebuild for vdr 1.6.0-1ctvdr7

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 12 May 2008 12:55:20 +0200

vdr-plugin-extrecmenu (1.1-6) experimental; urgency=low

  * Build-depend on vdr-dev (>= 1.6.0)
  * Switched Build-System to cdbs, Build-Depend on cdbs
  * Renamed XS-Vcs-* fields to Vcs-* in debian/control
  * Bumped Standards-Version to 3.7.3
  * Using COMPAT=5 now

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 04 Apr 2008 12:35:01 +0200

vdr-plugin-extrecmenu (1.1-5) experimental; urgency=low

  * Force rebuild for vdr 1.5.15

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 18 Feb 2008 21:07:51 +0100

vdr-plugin-extrecmenu (1.1-4) unstable; urgency=low

  * Release for vdrdevel 1.5.13

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 16 Jan 2008 10:31:31 +0100

vdr-plugin-extrecmenu (1.1-3) unstable; urgency=low

  * Release for vdrdevel 1.5.12

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 20 Nov 2007 23:46:16 +0100

vdr-plugin-extrecmenu (1.1-2) unstable; urgency=low

  * Release for vdrdevel 1.5.11

 -- Thomas Günther <tom@toms-cafe.de>  Tue,  6 Nov 2007 23:34:10 +0100

vdr-plugin-extrecmenu (1.1-1) unstable; urgency=low

  * New upstream release
  * Removed 90_MainMenuHooks.dpatch - fixed upstream
  * Removed 91_vdr1.5.4_extrecmenu-0.13p.dpatch - fixed upstream
  * Updated debian/copyright
  * Updated 01_dvdarchive-path.dpatch

 -- Tobias Grimm <tg@e-tobi.net>  Thu, 25 Oct 2007 18:08:27 +0200

vdr-plugin-extrecmenu (0.13-9) unstable; urgency=low

  * Release for vdrdevel 1.5.10

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 16 Oct 2007 23:50:52 +0200

vdr-plugin-extrecmenu (0.13-8) unstable; urgency=low

  * Release for vdrdevel 1.5.9

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 28 Aug 2007 01:01:02 +0200

vdr-plugin-extrecmenu (0.13-7) unstable; urgency=low

  * Release for vdrdevel 1.5.8

 -- Thomas Günther <tom@toms-cafe.de>  Thu, 23 Aug 2007 01:08:57 +0200

vdr-plugin-extrecmenu (0.13-6) unstable; urgency=low

  * Release for vdrdevel 1.5.6

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 14 Aug 2007 01:46:11 +0200

vdr-plugin-extrecmenu (0.13-5) unstable; urgency=low

  [ Thomas Günther ]
  * Fixed 91_vdr1.5.4_extrecmenu-0.13p.dpatch for compilation with VDR < 1.5.3

 -- Tobias Grimm <tg@e-tobi.net>  Wed, 11 Jul 2007 19:38:11 +0200

vdr-plugin-extrecmenu (0.13-4) experimental; urgency=low

  [ Thomas Schmidt ]
  * Added XS-Vcs-Svn and XS-Vcs-Browser fields to debian/control

  [ Thomas Günther ]
  * Release for vdrdevel 1.5.5
  * Added 91_vdr1.5.4_extrecmenu-0.13p.dpatch

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 27 Jun 2007 23:38:28 +0200

vdr-plugin-extrecmenu (0.13-3) experimental; urgency=low

  * Added 90_MainMenuHooks.dpatch

 -- Tobias Grimm <tg@e-tobi.net>  Sun, 11 Mar 2007 11:48:13 +0100

vdr-plugin-extrecmenu (0.13-2) unstable; urgency=low

  * Release for vdrdevel 1.5.1

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 27 Feb 2007 19:59:13 +0100

vdr-plugin-extrecmenu (0.13-1) unstable; urgency=low

  * New upstream release

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 16 Feb 2007 21:31:02 +0100

vdr-plugin-extrecmenu (0.12c-1) experimental; urgency=low

  [ Thomas Günther ]
  * Replaced VDRdevel adaptions in debian/rules with make-special-vdr
  * Adapted call of dependencies.sh and patchlevel.sh to the new location
    in /usr/share/vdr-dev/

  [ Tobias Grimm ]
  * New upstream release
  * Build-Depend on vdr-dev (>=1.4.5-1)
  * Updated 01_dvdarchive-path.dpatch

 -- Tobias Grimm <tg@e-tobi.net>  Sun, 14 Jan 2007 20:24:47 +0100

vdr-plugin-extrecmenu (0.12a-4) unstable; urgency=low

  * Build-Depend on vdr-dev (>=1.4.4-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  5 Nov 2006 00:00:41 +0100

vdr-plugin-extrecmenu (0.12a-3) unstable; urgency=low

  [ Thomas Schmidt ]
  * Build-Depend on vdr-dev (>=1.4.3-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 30 Sep 2006 16:28:11 +0200

vdr-plugin-extrecmenu (0.12a-2) unstable; urgency=low

  * Bumped Standards-Version to 3.7.2
  * Added note about the Debian Maintainers to debian/copyright
  * Build-Depend on vdr-dev (>=1.4.2-1)

 -- Thomas Schmidt <tschmidt@debian.org>  Mon, 28 Aug 2006 21:00:57 +0200

vdr-plugin-extrecmenu (0.12a-1) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release
    - Removed 03_fix-cmdsubmenu.dpatch

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 22 Jun 2006 00:23:04 +0200

vdr-plugin-extrecmenu (0.10-2) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.4.1-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun, 18 Jun 2006 15:56:22 +0200

vdr-plugin-extrecmenu (0.10-1) unstable; urgency=low

  * New upstream release
  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.4.0-1)
  * Tobias Grimm <tg@e-tobi.net>
    - Removed 90_APIVERSION.dpatch
    - Updated 01_dvdarchive-path.dpatch
  * Thomas Günther <tom@toms-cafe.de>
    - Removed 02_fix-jumpplay.dpatch (fixed upstream)
    - Updated 03_fix-cmdsubmenu.dpatch

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Wed,  3 May 2006 22:20:06 +0200

vdr-plugin-extrecmenu (0.9a-1) unstable; urgency=low

  * Thomas Günther <tom@toms-cafe.de>
    - New upstream release
    - Updated 02_fix-jumpplay.dpatch
    - Added 03_fix-cmdsubmenu.dpatch
    - Added 90_APIVERSION.dpatch
    - Build-Depend on vdr-dev (>= 1.3.48-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Mon, 24 Apr 2006 21:50:32 +0200

vdr-plugin-extrecmenu (0.9-2) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.3.47-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 20 Apr 2006 08:01:42 +0200

vdr-plugin-extrecmenu (0.9-1) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release
    - Updated 01_dvdarchive-path.dpatch
  * Thomas Günther <tom@toms-cafe.de>
    - Added 02_fix-jumpplay.dpatch
    - Build-Depend on vdr-dev (>= 1.3.46-1ctvdr2)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sat, 15 Apr 2006 11:47:01 +0200

vdr-plugin-extrecmenu (0.8a-2) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.3.46-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 13 Apr 2006 00:30:16 +0200

vdr-plugin-extrecmenu (0.8a-1) unstable; urgency=low

  * Thomas Günther <tom@toms-cafe.de>
    - New upstream release
    - Updated 01_dvdarchive-path.dpatch

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sun,  2 Apr 2006 19:58:00 +0200

vdr-plugin-extrecmenu (0.7-1) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.3.45-1)
  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Tue, 28 Mar 2006 19:01:55 +0200

vdr-plugin-extrecmenu (0.6a-1) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - New upstream release

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Sat, 18 Mar 2006 11:03:52 +0100

vdr-plugin-extrecmenu (0.6-1) unstable; urgency=low

  * Thomas Günther <tom@toms-cafe.de>
    - New upstream release

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Fri, 17 Mar 2006 00:08:47 +0100

vdr-plugin-extrecmenu (0.5-1) unstable; urgency=low

  * New upstream release

 -- Stefan Wagner <stefanwag@gmx.at>  Mon, 13 Mar 2006 19:34:52 +0100

vdr-plugin-extrecmenu (0.4-1) unstable; urgency=low

  * New upstream release

 -- Stefan Wagner <stefanwag@gmx.at>  Mon, 13 Mar 2006 13:58:56 +0100

vdr-plugin-extrecmenu (0.3-1) unstable; urgency=low

  * New upstream release

 -- Stefan Wagner <stefanwag@gmx.at>  Sun, 12 Mar 2006 18:47:47 +0100

vdr-plugin-extrecmenu (0.2-1) unstable; urgency=low

  * New upstream release

 -- Stefan Wagner <stefanwag@gmx.at>  Sat, 11 Mar 2006 20:56:51 +0100

vdr-plugin-extrecmenu (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Stefan Wagner <stefanwag@gmx.at>  Fri, 10 Mar 2006 13:20:31 +0100

